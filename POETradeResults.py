import os
import requests
from lxml import etree, html
from POETradeItem import Items, Item, Mod, ModTypes

class POETradeResults():

    def __init__(self, url):
        self.url = url

    def extract_items(self):
        tree = self.get_tree()
        items = Items()
        item_trees = tree.xpath('//tbody')
        for item_tree in item_trees:
            item = self.extract_item(item_tree)
            if item != None:
                items.add_item(item)

        return items

    def get_tree(self):
        if os.path.isfile(self.url):
            return self.load_page(self.url)
        elif self.url:
            return self.get_page(self.url)

    def get_page(self, url):
        page = requests.get(url)
        return html.fromstring(page.content)

    def load_page(self, path):
        with open(path, "r") as f:
            page = f.read()
            return html.fromstring(page)

    def parse_mod(self, name, value, text):
        mod_type = ModTypes.explicit
        name = name[1:]

        if '(implicit)' in name:
            name = name.replace('(implicit)', '').strip()
            mod_type = ModTypes.implicit
        elif '(enchant)' in name:
            name = name.replace('(enchant)', '').strip()
            mod_type = ModTypes.enchant
        elif '(pseudo)' in name:
            name = name.replace('(pseudo)', '').strip()
            mod_type = ModTypes.pseudo
        elif name[-1] == '@':
            name = name[:-1]
            mod_type = ModTypes.crafted

        return Mod(name, value, mod_type)

    def extract_item(self, item_tree):
        name = item_tree.get('data-name')
        buyout_cost = item_tree.get('data-buyout')
        pic_values = item_tree.xpath('.//span[@data-name="price_in_chaos"]/@data-value[1]')
        seller = item_tree.get('data-ign')
        if len(pic_values) == 0:
            return None
        price_in_chaos = -1 * float(pic_values[0])
        
        item = Item(name, buyout_cost, price_in_chaos, seller)

        # Get table stats
        row_n1 = item_tree.xpath('.//tr[@class="cell-first"][1]/th/text()')
        row_n2 = item_tree.xpath('.//tr[@class="cell-second"][1]/th/text()')
        row_v1 = item_tree.xpath('.//tr[@class="cell-first"][2]/td/@data-value')
        row_v2 = item_tree.xpath('.//tr[@class="cell-second"][2]/td/@data-value')
        table_stats_names = list(map(str.strip, row_n1 + row_n2))
        table_stats_values = list(map(float, row_v1 + row_v2))

        item.set_table_stats(table_stats_names, table_stats_values)

        # Get mods
        mods = item_tree.xpath('.//ul[@class="item-mods"]/li/ul/li[@data-name]')
        for mod in mods:
            name = mod.get('data-name')
            value = float(mod.get('data-value'))
            text = mod.text
            item.add_mod(self.parse_mod(name, value, text))

        return item
