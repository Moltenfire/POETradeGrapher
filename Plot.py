import plotly as py
import plotly.graph_objs as go
import os

def plot(x_data, y_data, x_name, y_name, title, hovertext):

    # Create a trace
    trace = go.Scatter(
        x = x_data,
        y = y_data,
        mode = 'markers',
        name = x_name,
        text = hovertext,
        hoverinfo='text'
    )

    layout= go.Layout(
        title=title,
        hovermode= 'closest',
        xaxis= dict(title= x_name),
        yaxis=dict(title= y_name),
        showlegend= False
    )

    data = [trace]

    fig = go.Figure(data=data, layout=layout)

    py.offline.plot(fig)
