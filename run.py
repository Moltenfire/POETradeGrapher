import argparse
from POETradeSearch import POETradeSearch
from ProcessSearch import ProcessSearch

def main(url, indexes, weights, all):
    search = POETradeSearch('http://poe.trade/search/' + url)
    # search = POETradeSearch('ngamahu.html')
    mods = search.get_mods()

    if len(indexes) > 0 or all:
        ps = ProcessSearch(search.get_items())
        if all:
            for mod in mods:
                ps.add_weight(mod)
        else:
            if len(weights) == 0:
                weights = [1] * len(indexes)
            for i, index in enumerate(indexes):
                ps.add_weight(mods[index], weights[i])
        ps.plot()
    else:
        i = 0
        for mod in mods:
            print("%2d" % i, mod)
            i += 1
        

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument("search") 	
    parser.add_argument('integers', metavar='N', type=int, nargs='*', help='mod indexes', default=[])
    parser.add_argument('-w', metavar='W', type=float, nargs='*', help='weights', default=[])
    parser.add_argument('-a', action='store_true')
    args = parser.parse_args()
    main(args.search, args.integers, args.w, args.a)
