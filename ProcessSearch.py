from collections import defaultdict
import Plot
import itertools

class ProcessSearch:

    def __init__(self, items):
        self.items = items
        self.weights = defaultdict(int)

    def add_weight(self, name, weight = 1):
        self.weights[name] = weight

    def total_weights(self):
        return sum(self.weights.values())

    def get_weighted_data(self):
        data = []
        first = True
        for name, weight in self.weights.items():
            d = list(map(lambda x: x * weight, self.items.get_data(name)))
            if first:
                first = False
                data = d
            else:
                data = [sum(x) for x in itertools.zip_longest(data, d)]

        return list(data)

    def weights_text(self):
        return ", ".join(self.weights.keys())

    def plot(self):
        if self.total_weights() == 0:
            return
        
        cost = self.items.get_cost()
        weighted_data = self.get_weighted_data()
        inverse_weighted_data = [i/j for i,j in zip(weighted_data, cost)]
        hovertext = self.items.get_text()

        title = self.weights_text()
        x_text = self.weights_text()
        y_text = "Weighted Value/Chaos"

        Plot.plot(weighted_data, inverse_weighted_data, x_text, y_text, title, hovertext)
