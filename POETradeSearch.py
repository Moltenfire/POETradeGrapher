from POETradeResults import POETradeResults

class POETradeSearch():

    def __init__(self, url):
        self.page = POETradeResults(url)

    def get_items(self):
        return self.page.extract_items()

    def get_mods(self, stats = True):
        items = self.get_items()
        mods = []
        pseudo_mods = list(items.get_pseudo_mods())
        if len(pseudo_mods) > 0:
            for mod in pseudo_mods:
                mods.append(mod)
        else:
            for name, number, percent in items.get_most_common_mods():
                mods.append(name)
        
        if stats:
            for stat, value in items.items[0].get_stats():
                mods.append(stat)
        return mods

    
