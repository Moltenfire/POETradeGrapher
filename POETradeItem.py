from enum import Enum
from collections import Counter

class ModTypes(Enum):
    implicit = 1
    explicit = 2
    crafted = 3
    enchant = 4
    pseudo = 5

class Mod:
    def __init__(self, name, value, mod_type):
        self.name = name
        self.value = value
        self.mod_type = mod_type

    def __str__(self):
        # return self.name + " " + str(self.value) + " " + str(self.mod_type)
        return self.name.replace('#', "%d" % self.value)

    # def get_text(self):
    #     text = self.name.replace('#', self.value)

class Item:

    def __init__(self, name, cost, price_in_chaos, seller):
        self.name = name
        self.cost = cost
        self.seller = seller
        self.price_in_chaos = price_in_chaos
        self.stats = {}
        self.mods = {}

    def set_table_stats(self, names, values):
        for name, value in zip(names, values):
            self.stats[name] = value

    def add_mod(self, mod):
        if mod.name not in self.mods:
            self.mods[mod.name] = mod
        else:
            self.mods[mod.name].value += mod.value
            self.mods[mod.name].mod_type = ModTypes.explicit

    def get_mods(self, mod_types = []):
        for name, mod in self.mods.items():
            if len(mod_types) == 0 or mod.mod_type in mod_types:
                yield mod

    def get_stats(self):
        for name, value in self.stats.items():
            if value != 0:
                yield name, value

    def get_cost(self):
        return self.price_in_chaos

    def get_data(self, name):
        if name in self.stats:
            return self.stats[name]
        elif name in self.mods:
            return self.mods[name].value
        else:
            return 0

    def get_text(self):

        text = "%s<br>%s<br>%s<br>----------------" % (self.name, self.cost, self.seller)

        for mod in self.get_mods([ModTypes.implicit, ModTypes.explicit, ModTypes.crafted, ModTypes.enchant]):
            text += "<br>%s" % str(mod)

        pseudo_mods = list(self.get_mods([ModTypes.pseudo]))
        if len(pseudo_mods) > 0:
            text += "<br>----------------"
            for mod in pseudo_mods:
                text += "<br>%s (pseudo)" % str(mod)

        stats = list(self.get_stats())
        if len(stats) > 0:
            text += "<br>----------------"
            for name, value in stats:
                text += "<br>%s: %0.3f" % (name, value)

        return text

    def __str__(self):
        return self.name + ": " + self.cost + ": " + str(self.price_in_chaos)

class Items:

    def __init__(self):
        self.items = []
    
    def add_item(self, item):
        self.items.append(item)

    def get_cost(self):
        data = []
        for item in self.items:
            data.append(item.get_cost())
        return data

    def get_text(self):
        data = []
        for item in self.items:
            data.append(item.get_text())
        return data

    def get_data(self, name):
        data = []
        for item in self.items:
            data.append(item.get_data(name))
        return data
        
    def get_pseudo_mods(self):
        for mod in self.items[0].get_mods([ModTypes.pseudo]):
            yield mod.name

    def get_most_common_mods(self, n = 10):
        mods = Counter()
        total = len(self.items)

        for item in self.items:
            for mod in item.get_mods():
                mods[mod.name] += 1

        for name, count in mods.most_common(n):
            yield name, count, count/total
